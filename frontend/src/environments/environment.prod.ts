const url: string = "http://52.86.239.14:7000";

export const environment = {
  production: true,
  url: "http://52.86.239.14:7000",
  test: "/test",
  codes: "/codes",
  values: "/values",
  masterValues: "/master_values",
  addMasterValue: "/add_master_value",
  removeMasterValue: "/remove_master_value",
};
