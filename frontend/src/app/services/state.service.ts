import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RequestsService } from './requests.service';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  public pageName: string;
  public codesValues = new BehaviorSubject({ codes: [], values: [] });

  constructor(private requests: RequestsService) { }


  /**
   * sets current workin page name
   * @param pageName page name
   */
  public setPageName(pageName): void {
    this.pageName = pageName;
  }


  /**
   * gets current working page name
   * @return page name
   */
  public getPageName(): string {
    return this.pageName;
  }

  /**
   * retrieves and organizes codes & values
   */
  public async loadCodesValues() {
    // rqt params
    var data = {
      url: "codes",
      params: {
        params: {},
        proc: "",
        procs: ["GetCodes", "GetValues", "GetMasterCodes"]
      }
    }

    var codes = await this.requests.postRequest(data).toPromise();
    var codesValues = { codes: codes["GetCodes"], values: codes["GetValues"] };

    // map for linking {mastercodes: codes}
    var codesMapping = {}
    codesValues.codes.forEach((code, idx) => { codesMapping[code.id] = idx; })

    codes["GetMasterCodes"].forEach(value => {
      let v = { abv: "", name: "", id: "" }
      if (value.abv !== null) {
        v = { abv: value.abv, name: value.value_name, id: value.value_id }
      }

      if ('values' in codesValues.codes[codesMapping[value.code_id]]) {
        codesValues.codes[codesMapping[value.code_id]]['values'].push(v);
      } else {
        codesValues.codes[codesMapping[value.code_id]]['values'] = [v];
      }
    })

    this.setCodesValues(codesValues);
  }

  /**
   * Set codes & values
   * @param  data codes & values
   */
  public setCodesValues(data) {
    this.codesValues.next(data);
  }

  /**
   * returns the codes & values
   * @return codesValues BehaviorSubject
   */
  public getCodesValues() {
    return this.codesValues;
  }

}
