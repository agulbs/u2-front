// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// const url: string = "http://52.86.239.14:7000";

export const environment = {
  production: false,
  // url: "http://52.86.239.14:7000",
  url: "http://3.236.39.30:5000",
  // test: "/test",
  // codes: "/codes",
  // values: "/values",
  // masterValues: "/master_values",
  // addMasterValue: "/add_master_value",
  // removeMasterValue: "/remove_master_value",
  login: "/login",
  codes: "/codes",
  addCode: "/code/add",
  values: "/values",
  addValue: "/value/add",
  masterValues: "/master-values",
  updateMasterValue: "/master-value/update",
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
