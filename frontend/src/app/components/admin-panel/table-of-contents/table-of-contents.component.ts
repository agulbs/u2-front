import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-of-contents',
  templateUrl: './table-of-contents.component.html',
  styleUrls: ['./table-of-contents.component.css']
})
export class TableOfContentsComponent implements OnInit {
  public tableOfContents = [
    {
      name: "Generate Token",
      type: "POST",
      color: "#f77e8a",
      description: "- Generates a JWT auth token.",
      requirements: "- username & password: string"
    },
    // {
    //   name: "Validate API",
    //   type: "GET",
    //   color: "#28a745",
    //   description: "- Validates the state of the API.",
    //   requirements: ""
    // },
    {
      name: "Get UDC Codes",
      type: "GET",
      color: "#28a745",
      description: "- Display various udc-codes.",
      requirements: ""
    },
    {
      name: "Get UDC Values",
      type: "GET",
      color: "#28a745",
      description: "- Display various values for udc-codes.",
      requirements: ""
    },
    {
      name: "Get Master Values",
      type: "GET",
      color: "#28a745",
      description: "- Validates the state of the API.",
      requirements: ""
    },
    {
      name: "Add a Master Value",
      type: "POST",
      color: "#f77e8a",
      description: "- Add a master Value.",
      requirements: "- Requires an id: integer"
    },
    {
      name: "Delete a Master Value",
      type: "POST",
      color: "#f77e8a",
      description: "- Remove a master Value.",
      requirements: "- Requires an id: integer"
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
