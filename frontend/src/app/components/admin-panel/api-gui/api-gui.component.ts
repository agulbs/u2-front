import { Component, OnInit } from '@angular/core';
import { StateService } from '../../../services/state.service';
import { RequestsService } from '../../../services/requests.service';
import { environment } from "../../../../environments/environment";
import { BehaviorSubject } from 'rxjs';



@Component({
  selector: 'app-api-gui',
  templateUrl: './api-gui.component.html',
  styleUrls: ['./api-gui.component.css']
})
export class ApiGuiComponent implements OnInit {

  public token;
  public urls = environment;
  public crudMap = { c: "added", d: "deleted" };
  public tableOfConents = [
    { name: "Validate API" },
    { name: "Get UDC Codes" },
    { name: "Get UDC Values" },
    { name: "Get Master Values" },
    { name: "Add a Master Value" },
    { name: "Delete a Master Value" },
  ];
  public adminAbilities = {
    auths: [
      {
        name: "VerifyLogin",
        placeHolder: "Generate Auth Token",
        description: "Generates an authroization token provided a username & password.",
        url: "login",
        route: this.urls.url + this.urls["login"],
        headers: [
          { field: "Content-Type", type: "application/json", description: "Request paylaod" }
        ],
        params: [
          { field: "username", type: "string", description: "username to authenticate" },
          { field: "password", type: "string", description: "password to authenticate user" }
        ],
        inputs: [
          { type: "text", placeHolder: "username", value: "" },
          { type: "password", placeHolder: "password", value: "" }
        ]
      },
    ],
    gets: [
      // {
      //   name: "validateApi",
      //   placeHolder: "Validate API",
      //   description: "Validates the state of the API.",
      //   url: "test",
      //   route: this.urls.url + this.urls["test"],
      //   headers: [
      //     { field: "Content-Type", type: "application/json", description: "Request paylaod" },
      //     { field: "Authorization", type: "Bearer", description: "Authorization Token" },
      //   ],
      // },
      {
        name: "GetCodes",
        placeHolder: "Get UDC Codes",
        description: "Display various udc-codes.",
        url: "codes",
        route: this.urls.url + this.urls["codes"],
        headers: [
          { field: "Content-Type", type: "application/json", description: "Request paylaod" },
          { field: "Authorization", type: "Bearer", description: "Authorization Token" }
        ],
        showToken: true
      },
      {
        name: "GetValues",
        placeHolder: "Get UDC Values",
        description: "Display various values for udc-codes.",
        url: "values",
        route: this.urls.url + this.urls["values"],
        headers: [
          { field: "Content-Type", type: "application/json", description: "Request paylaod" },
          { field: "Authorization", type: "Bearer", description: "Authorization Token" },
        ],
      },
      {
        name: "GetMasterValues",
        placeHolder: "Get Master Values",
        description: "Gets all the master values.",
        url: "masterValues",
        route: this.urls.url + this.urls["masterValues"],
        headers: [
          { field: "Content-Type", type: "application/json", description: "Request paylaod" },
          { field: "Authorization", type: "Bearer", description: "Authorization Token" },
        ],
      }
    ],
    posts: [
      {
        name: "AddMasterValue",
        placeHolder: "Add Master Value",
        inputPlaceHolder: "id: id of master value to add",
        description: "Add a master Value.",
        url: "updateMasterValue",
        route: this.urls.url + this.urls["updateMasterValue"],
        id: '',
        type: "number",
        crud: 'c',
        params: [
          { field: "id", type: "integer", description: "integer with value > 0" },
        ],
        headers: [
          { field: "Content-Type", type: "application/json", description: "Request paylaod" },
          { field: "Authorization", type: "Bearer", description: "Authorization Token" },
        ],
        inputs: [
          { type: "number", placeHolder: "id of master value to add", txt: "id", value: "" },
        ]
      },
      {
        name: "RemoveMasterValue",
        placeHolder: "Remove Master Value",
        inputPlaceHolder: "id: id of master value to delete",
        description: "Remove a master Value.",
        url: "updateMasterValue",
        route: this.urls.url + this.urls["updateMasterValue"],
        id: '',
        type: "number",
        crud: 'd',
        params: [
          { field: "id", type: "integer", description: "integer with value > 0" },
        ],
        headers: [
          { field: "Content-Type", type: "application/json", description: "Request paylaod" },
          { field: "Authorization", type: "Bearer", description: "Authorization Token" },
        ],
        inputs: [
          { type: "number", placeHolder: "id of master value to delete", txt: "id", value: "" },
        ]
      }
    ]
  };

  public _tokens = new BehaviorSubject([]);

  constructor(public state: StateService, private requests: RequestsService) {
    this._tokens.subscribe((data) => {
      console.log(data)
      this.token = data;
    });
  }


  ngOnInit() {
    this.setTokens({ accessToken: "", refreshToken: "" })
    this.state.setPageName("Admin Panel");
  }

  public setTokens(tokens) {
    this._tokens.next(tokens);
  }

  public getTokens() {
    return this._tokens;
  }


  /**
   * makes get requests
   * @param  route url link
   * @param  name  id field of elements
   */
  public getData(route, name) {
    // if (this.token['accessToken'].length == 0) {
    //   alert('generate a token');
    //   return false;
    // }
    //
    // let data = {
    //   url: route,
    //   tokens: this.token,
    //   params: { params: {}, proc: name },
    // }

    let data = {
      url: route,
      params: { params: {}, proc: name },
    }

    this.requests.postRequest(data).subscribe(
      data => { this.renderJson(name, data); },
      err => { console.log(err); }
    );






  }


  /**
   * make post request
   * @param  value object for request
   */
  public updateData(value) {
    let c = this.crudMap[value.crud];
    value.id = parseInt(value.id);

    if (value.id < 1) {
      alert("Please verify that the ID is valid.");
      return false;
    }

    let data = {
      url: value.url,
      params: { params: { id: value.id }, proc: value.name }
    }

    this.requests.postRequest(data).subscribe(
      data => {
        let json = { id: value.id };
        json[c] = data;
        this.renderJson(value.name, json);
      }
    );
  }


  public handleAuths(value) {
    let data = {
      url: value.url,
      params: { params: {}, proc: value.name }
    }

    value.inputs.forEach(value => {
      data.params.params[value.placeHolder] = value.value;
    })

    console.log(data)

    this.requests.postRequest(data).subscribe(
      data => {

        this.setTokens({ accessToken: data['access_token'], refreshToken: data['refresh_token'] })

        this.renderJson(value.name, data);
      }
    );
  }


  /**
   * renders JSON to template
   * @param name html element id
   * @param json json to render
   */
  public renderJson(name, json): void {
    document.getElementById(name + "Pre").innerHTML = JSON.stringify(json, undefined, 2);
    document.getElementById(name + "Div").style.display = "block";
  }


  /**
   * closes rendered data
   * @param name html element id
   */
  public closeData(name): void {
    document.getElementById(name + "Pre").innerHTML = "";
    document.getElementById(name + "Div").style.display = "none";
  }
}
