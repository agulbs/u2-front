import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApiGuiComponent } from './api-gui.component';

describe('ApiGuiComponent', () => {
  let component: ApiGuiComponent;
  let fixture: ComponentFixture<ApiGuiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApiGuiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApiGuiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
