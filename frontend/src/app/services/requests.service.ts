import { Injectable } from '@angular/core';
import { Observable } from "rxjs/internal/Observable";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  private urls = environment;
  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
    })
  };


  constructor(private http: HttpClient) { }


  /**
   * makes post request
   * @param  params url & param data
   * @return        observable
   */
  public postRequest(params): Observable<any> {
    if ("tokens" in params) {
      this.httpOptions.headers = this.httpOptions.headers.set('Authorization', "Bearer " + params.tokens.accessToken);
    }

    let url = this.urls.url + this.urls[params.url];
    return this.http.post(url, params.params, this.httpOptions);
  }


  /**
   * makes post request
   * @param  params url
   * @return        observable
   */
  public getRequest(params): Observable<any> {
    if ("tokens" in params) {
      this.httpOptions.headers = this.httpOptions.headers.set('Authorization', "Bearer " + params.tokens.accessToken);
    }

    let url = this.urls.url + this.urls[params.url];
    return this.http.get(url, this.httpOptions);
  }


}
