import { Component, OnInit } from '@angular/core';
import { StateService } from '../../services/state.service';
@Component({
  selector: 'app-hoc',
  templateUrl: './hoc.component.html',
  styleUrls: ['./hoc.component.css']
})
export class HocComponent implements OnInit {

  constructor(public state: StateService) { }

  ngOnInit() {
  }

}
